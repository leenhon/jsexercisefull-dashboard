import { layThongTinTuForm } from "../controllers/AddProductControllers.js"
import { showThongTin } from "../controllers/AddProductControllers.js"
import { Product } from "../model/Product.js";

const Base_URL = "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products";

document.getElementById("btnThemSP").addEventListener("click", function () {
    let thongTinSP = layThongTinTuForm();
    let { tenSP, hangSX,giaSP,screenSP,camFront,camBack,soLuong,hinhSP,moTa } = thongTinSP;

    let sanPham = new Product(tenSP, hangSX,giaSP,screenSP,camFront,camBack,soLuong,hinhSP,moTa);
    console.log(sanPham);
    showThongTin(sanPham);

    axios({
        url: Base_URL,
        method: "POST",
        data: sanPham,
    })
        .then(function (res) {
            console.log("Thêm sản phẩm mới", res);
            alert("Thêm sản phẩm thành công!");
        })
        .catch(function (err) {
            console.log("not create", err);
        });
})